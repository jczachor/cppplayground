#include "Pracownik.h"


Pracownik::Pracownik(const char * im, const char * naz, int dzien, int miesiac, int rok)
	:m_nIDZatrudnienia(m_nID)
{
	this->m_Imie.Ustaw(im);
	this->m_Nazwisko.Ustaw(naz);
	this->m_DataUrodzenia.Ustaw(dzien, miesiac, rok);
	this->m_nID++;
}
Pracownik::Pracownik(const Pracownik & wzor)
	:m_nIDZatrudnienia(m_nID)
{
	this->m_Imie = wzor.Imie();
	this->m_Nazwisko = wzor.Nazwisko();
	this->m_DataUrodzenia = wzor.m_DataUrodzenia;
	m_nID++;
}

Pracownik::~Pracownik()
{
}

const char * Pracownik::Imie() const
{
	return m_Imie.Zwroc();
}

const char * Pracownik::Nazwisko() const
{
	return m_Nazwisko.Zwroc();
}

void Pracownik::Imie(const char * nowe_imie)
{
	m_Imie.Ustaw(nowe_imie);
}

void Pracownik::Nazwisko(const char * nowe_nazwisko)
{
	m_Nazwisko.Ustaw(nowe_nazwisko);
}

void Pracownik::DataUrodzenia(int nowy_dzien, int nowy_miesiac, int nowy_rok)
{
	m_DataUrodzenia.Ustaw(nowy_dzien, nowy_miesiac, nowy_rok);
}

void Pracownik::Wypisz() const
{
	m_Imie.Wypisz();
	cout << "	";
	m_Nazwisko.Wypisz();
	cout << "	";
	m_DataUrodzenia.Wypisz();
	cout << endl;
}

void Pracownik::Wpisz()
{
	cout << "podaj imie ";
	m_Imie.Wpisz();
	cout << "podaj nazwisko ";
	m_Nazwisko.Wpisz();
	cout << "podaj date ";
	m_DataUrodzenia.Wpisz();
}

int Pracownik::SprawdzImie(const char * por_imie) const
{
	return m_Imie.SprawdzNapis(por_imie);
}

int Pracownik::SprawdzNazwisko(const char * por_nazwisko) const
{
	return m_Nazwisko.SprawdzNapis(por_nazwisko);
}

int Pracownik::Porownaj(const Pracownik & wzorzec) const
{
	if (SprawdzImie(wzorzec.Imie()) == 0 && SprawdzNazwisko(wzorzec.Nazwisko()) == 0 && m_DataUrodzenia.Porownaj(wzorzec.m_DataUrodzenia) == 0)
	{
		return 0;
	}
	else if (SprawdzNazwisko(wzorzec.Nazwisko()) > 0)
	{
		return 1;
	}
	else if (SprawdzNazwisko(wzorzec.Nazwisko()) < 0)
	{
		return -1;
	}
	else if (SprawdzImie(wzorzec.Imie()) > 0)
	{
		return 1;
	}
	else if (SprawdzImie(wzorzec.Imie())  < 0)
	{
		return -1;
	}
	else if (m_DataUrodzenia.Porownaj(wzorzec.m_DataUrodzenia) > 0)
	{
		return 1;
	}
	else if (m_DataUrodzenia.Porownaj(wzorzec.m_DataUrodzenia) < 0)
	{
		return -1;
	};
}

Pracownik & Pracownik::operator=(const Pracownik & wzor)
{
	if (this != &wzor) {
		this->m_Imie = wzor.Imie();
		this->m_Nazwisko = wzor.Nazwisko();
		this->m_DataUrodzenia = wzor.m_DataUrodzenia;
	}
	return *this;
}

bool Pracownik::operator==(const Pracownik & wzor)
{
	return Porownaj(wzor)==0;
}

ostream & operator<<(ostream & wy, const Pracownik & p)
{
	wy << p.m_Imie << "  " << p.m_Nazwisko << "  " << p.m_DataUrodzenia;
	return wy;
}

istream & operator>>(istream & we, Pracownik & p)
{
	we >> p.m_Imie >> p.m_Nazwisko >> p.m_DataUrodzenia;
	return we;
}
