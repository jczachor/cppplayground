#include <iostream>
#include "Pracownik.h"
#include "ListaPracownikow.h"
#include "Napis.h"


using namespace std;
int Pracownik::m_nID = 0;
int main() {
	Pracownik p1, p2, p3, p4;
	p1.DataUrodzenia(1, 2, 2000);
	p2.DataUrodzenia(2, 3, 2001);
	p3.DataUrodzenia(5, 6, 2004);
	p4.DataUrodzenia(4, 5, 2003);
	p1.Imie("aaaa");
	p2.Imie("bbbb");
	p3.Imie("dddd");
	p4.Imie("cccc");
	p1.Nazwisko("111");
	p2.Nazwisko("222");
	p3.Nazwisko("444");
	p4.Nazwisko("333");

	cout << "normalne wypisanie pracownikow" << endl;
	cout << p1 << p2 << p3 << p4;

	cout << "dodanie do listy i wypisanie juz posortowanych" << endl;
	ListaPracownikow lista;
	lista.Dodaj(p1);
	lista.Dodaj(p2);
	lista.Dodaj(p3);
	lista.Dodaj(p4);
	lista.WypiszPracownikow();

	cout << "wypisanie pracownika aaaa 111" << endl;
	if(lista.Szukaj("111", "aaaa") != nullptr)
	lista.Szukaj("111", "aaaa")->Wypisz();

	cout << "usuwanie pierwszego i trzeciego" << endl;
	lista.Usun(*lista.Szukaj("111", "aaaa"));
	lista.Usun(*lista.Szukaj("333", "cccc"));
	lista.WypiszPracownikow();

	cout << "TESTOWANIE NAPISOW" << endl;
	Napis n1("kot"), n2("pies"), n3(n2);
	cout << "wypisz" << endl;
	cout << n1 << n2 << n3 << endl;
	cout << "porownanie n2 i n3" << endl;
	if (n2 == n3) cout << "n2 i n3 sa rowne" << endl;
	cout << "porownanie n1 i n3" << endl;
	if (!(n1 == n3)) cout << "n1 i n3 nie sa rowne" << endl;
	cout << "podaj nowy napis n1" << endl;
	cin >> n1;
	cout << n1 << endl;

	system("pause");
}
