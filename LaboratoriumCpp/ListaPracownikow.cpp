#include "ListaPracownikow.h"



ListaPracownikow::ListaPracownikow()
{
	m_pPoczatek = NULL;
	this->m_nLiczbaPracownikow = 0;
}


ListaPracownikow::~ListaPracownikow()
{
	Pracownik* tmp = m_pPoczatek;
	Pracownik* usun;
	while (tmp) {
		usun = tmp;
		tmp = tmp->m_pNastepny;
		delete usun;
	}
	m_pPoczatek = NULL;
	m_nLiczbaPracownikow = 0;
}

void ListaPracownikow::Dodaj(const Pracownik & p)
{
	Pracownik* nowy = new Pracownik(p);
	if (m_nLiczbaPracownikow == 0) {
		m_pPoczatek = nowy;
		m_nLiczbaPracownikow++;
	}
	else {
		Pracownik* tmp = m_pPoczatek;
		for (int i = 0; i < m_nLiczbaPracownikow; i++) {
			if (tmp->Porownaj(*nowy) == -1) {

				if (tmp->m_pNastepny) {	
					tmp = tmp->m_pNastepny;
				}
				else {
					tmp->m_pNastepny = nowy;
					m_nLiczbaPracownikow++;
					break;
				}

			}
			else if (tmp->Porownaj(*nowy) == 1) {			
				Pracownik* tmp2 = m_pPoczatek;				
				if (tmp == tmp2) {							
					if (!m_pPoczatek->m_pNastepny) {		
						tmp2->m_pNastepny = NULL;			
					}
					m_pPoczatek = nowy;						
					nowy->m_pNastepny = tmp;
					m_nLiczbaPracownikow++;
				}
				else {										
					while (tmp2->m_pNastepny != tmp) {
						tmp2 = tmp2->m_pNastepny;
					}
					nowy->m_pNastepny = tmp;
					tmp2->m_pNastepny = nowy;
					m_nLiczbaPracownikow++;
					break;

				}
				break;
			}
			else {
				break;
			}
		}

	}
}

void ListaPracownikow::Usun(const Pracownik & wzorzec)
{
	Pracownik* tmp = m_pPoczatek;
	Pracownik* nowy = new Pracownik(wzorzec);
	if (!tmp->Porownaj(*nowy)) {
		m_pPoczatek = tmp->m_pNastepny;
		delete tmp;
		m_nLiczbaPracownikow--;
	}
	else {
		while (tmp->m_pNastepny->Porownaj(*nowy)) {	
			tmp = tmp->m_pNastepny;
		}
		Pracownik* usun = tmp->m_pNastepny;
		tmp->m_pNastepny = tmp->m_pNastepny->m_pNastepny;
		delete usun;
		m_nLiczbaPracownikow--;
	}
}

void ListaPracownikow::WypiszPracownikow() const
{
	Pracownik* tmp = m_pPoczatek;
	cout << "Lista " << m_nLiczbaPracownikow << " pracownikow:" << endl;
	while (tmp) {
		tmp->Wypisz();
		tmp = tmp->m_pNastepny;
	}
}

const Pracownik * ListaPracownikow::Szukaj(const char * nazwisko, const char * imie) const
{
	Pracownik* tmp = m_pPoczatek;
	for (int i = 0; i<m_nLiczbaPracownikow; i++)
	{
		if (!tmp->SprawdzNazwisko(nazwisko) && !tmp->SprawdzImie(imie)) {
			return tmp;
			break;
		}
		tmp = tmp->m_pNastepny;
	}
	return nullptr;
}
