#include "Data.h"


Data::Data(int d, int m, int r)
{
	this->m_nDzien = d;
	this->m_nMiesiac = m;
	this->m_nRok = r;
	Koryguj();
}

void Data::Ustaw(int d, int m, int r)
{
	this->m_nDzien = d;
	this->m_nMiesiac = m;
	this->m_nRok = r;
	Koryguj();
}

int Data::Dzien() const
{
	return this->m_nDzien;
}

int Data::Miesiac() const
{
	return this->m_nMiesiac;
}

int Data::Rok() const
{
	return this->m_nRok;
}

void Data::Wypisz() const
{
	cout << this->m_nDzien << "-" << this->m_nMiesiac << "-" << this->m_nRok;
}

void Data::Wpisz()
{
	cin >> this->m_nDzien >> this->m_nMiesiac >> this->m_nRok;
}

int Data::Porownaj(const Data & wzor) const
{
	if (wzor.m_nRok>m_nRok)
	{
		return 1;
	}
	else if (wzor.m_nRok < m_nRok)
	{
		return -1;
	}
	else if (wzor.m_nMiesiac>m_nMiesiac)
	{
		return 1;
	}
	else if (wzor.m_nMiesiac < m_nMiesiac)
	{
		return -1;
	}
	else if (wzor.m_nDzien>m_nDzien)
	{
		return 1;
	}
	else if (wzor.m_nDzien < m_nDzien)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

void Data::Koryguj()
{
	if (m_nDzien<1)
	{
		m_nDzien = 1;
	}
	if (m_nDzien>29 && m_nMiesiac == 2 && (m_nRok % 4 == 0 && m_nRok % 100 != 0 || m_nRok % 400 == 0))
	{
		m_nDzien = 28;
	}
	else if (m_nDzien>30)
	{
		m_nDzien = 29;
	}
	if (m_nMiesiac<1)
	{
		m_nMiesiac = 1;
	}
	if (m_nMiesiac> 12)
	{
		m_nMiesiac = 12;
	}
	if (m_nDzien>31 && (m_nMiesiac == 1 || m_nMiesiac == 3 || m_nMiesiac == 5 || m_nMiesiac == 6 || m_nMiesiac == 7 || m_nMiesiac == 9 || m_nMiesiac == 11))
	{
		m_nDzien = 31;
	}
	else if (m_nDzien>30)
	{
		m_nDzien = 30;
	}
}

ostream & operator<<(ostream & wy, const Data & d)
{
	wy << d.m_nDzien << "-" << d.m_nMiesiac << "-" << d.m_nRok << endl;
	return wy;
}

istream & operator>>(istream & we, Data & d)
{
	we >> d.m_nDzien >> d.m_nMiesiac >> d.m_nRok;
	return we;
}
