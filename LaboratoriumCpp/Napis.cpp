#include "Napis.h"


Napis::Napis(const char * nap)
{
	this->m_nDl = strlen(nap);
	this->m_pszNapis = new char[m_nDl+1];
	strcpy(this->m_pszNapis, nap);
}

Napis::Napis(const Napis & wzor)
{
	this->m_nDl = wzor.m_nDl;
	this->m_pszNapis = new char[m_nDl + 1];
	strcpy(this->m_pszNapis, wzor.m_pszNapis);
}

Napis::~Napis()
{
	delete[] this->m_pszNapis;
}

const char * Napis::Zwroc() const
{
	return this->m_pszNapis;
}

void Napis::Ustaw(const char * nowy_napis)
{
	delete[] this->m_pszNapis;
	this->m_nDl = strlen(nowy_napis);
	this->m_pszNapis = new char[m_nDl+1];
	strcpy(this->m_pszNapis, nowy_napis);
}

void Napis::Wypisz() const
{
	cout << this->m_pszNapis;
}

void Napis::Wpisz()
{
	cin >> m_pszNapis;
}

int Napis::SprawdzNapis(const char * por_napis) const
{
	return strcmp(this->m_pszNapis, por_napis);
}

Napis & Napis::operator=(const Napis & wzor)
{
	if (this != &wzor) {
	delete[] this->m_pszNapis;
	this->m_nDl = wzor.m_nDl;
	this->m_pszNapis = new char[m_nDl + 1];
	strcpy(this->m_pszNapis, wzor.m_pszNapis);
	}
	return *this;
}

bool Napis::operator==(const Napis & wzor) const
{
	return (strcmp(this->m_pszNapis, wzor.m_pszNapis) == 0);
}

ostream & operator<<(ostream & wy, const Napis & p)
{
	wy << p.m_pszNapis << " ";
	return wy;
}

istream & operator>>(istream & we, Napis & p)
{
	we >> p.m_pszNapis;
	return we;
}
